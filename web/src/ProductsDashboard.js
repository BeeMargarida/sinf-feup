import React, { Component } from 'react';
import { Grid, Segment, GridRow, GridColumn } from 'semantic-ui-react';
import { HorizontalBar, Line, Doughnut } from 'react-chartjs-2';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import axios from 'axios';
import './MainDashboard.css';
import './ProductsDashboard.css';

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

class ProductsDashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            options: [
                { key: 'DEC_2018', value: 'DEC_2018', text: 'Dezembro 2018' }
            ],
            products: [],
            top10Products: [],
            top5ProductsUnits: [],
            productsSaleCost: [],
            sales: [],
            inventoryValue: 0,
            selectedMonth: { label: months[new Date().getMonth()], value: new Date().getMonth() },
            months: [
                { label: "January", value: 0 },
                { label: "February", value: 1 },
                { label: "March", value: 2 },
                { label: "April", value: 3 },
                { label: "May", value: 4 },
                { label: "June", value: 5 },
                { label: "July", value: 6 },
                { label: "August", value: 7 },
                { label: "September", value: 8 },
                { label: "October", value: 9 },
                { label: "November", value: 10 },
                { label: "December", value: 11 }
            ]
        }

        this.handleMonthChange = this.handleMonthChange.bind(this);
        this.getProdInfo = this.getProdInfo.bind(this);
        this.getTop10Products = this.getTop10Products.bind(this);
        this.getTop5ProductsUnits = this.getTop5ProductsUnits.bind(this);
        this.getSaleCostData = this.getSaleCostData.bind(this);
    }

    async componentDidMount() {

        if (this.props.token !== null) {
            await this.getTop10Products();
            this.getTop5ProductsUnits();
            this.getSaleCostData();
            this.getSales();
        }

    }

    async componentDidUpdate(prevProps, prevState) {
        if (prevProps.token !== this.props.token) {
            await this.getTop10Products();
            this.getTop5ProductsUnits();
            this.getSaleCostData();
            this.getSales();
        }

        if (prevState.selectedMonth.label !== this.state.selectedMonth.label) {
            await this.getTop10Products();
            this.getTop5ProductsUnits();
            this.getSaleCostData();
            this.getSales();
        }
    }

    handleMonthChange(event) {
        this.setState({ selectedMonth: event });
    }

    async getTop10Products() {

        await axios.get('http://localhost:3030/getTop10Products?month=' + this.state.selectedMonth.value)
            .then((res) => {
                this.setState({ top10Products: res.data })
            })
            .catch((err) => console.log(err));

    }

    async getTop5ProductsUnits() {

        await axios.get('http://localhost:3030/getTop5ProductsUnits?month=' + this.state.selectedMonth.value)
            .then((res) => {
                this.setState({ top5ProductsUnits: res.data })
            })
            .catch((err) => console.log(err));

    }

    getSaleCostData() {

        let productsSaleCost = [];

        for (let i = 0; i < this.state.top10Products.length; i++) {

            let top10Prod = this.state.top10Products[i];
            for (let j = 0; j < this.props.products.length; j++) {

                if (top10Prod.name === this.props.products[j].Artigo) {

                    productsSaleCost.push({
                        name: top10Prod.name,
                        sale: this.props.products[j].PVP1,
                        cost: this.props.products[j].PCMedio
                    });
                    break;
                }
            }

        }

        this.setState({ productsSaleCost });

    }

    getProdInfo(prodName) {
        for (let i = 0; i < this.props.products.length; i++) {
            if (this.props.products[i].Artigo === prodName) {
                return { PVP1: this.props.products[i].PVP1, StkActual: this.props.products[i].StkActual }
            }
        }
    }

    getSales() {
        
        axios.get('http://localhost:3030/getAllSales')
            .then((res) => this.setState({ sales: res.data }))
            .catch((err) => console.log(err))

    }

    getPrettyNumber(number) {
        if (number < 1000)
            return number.toFixed(2) + " €";
        else if (number > 1000 && number < 1000000)
            return (number / 1000).toFixed(2) + "K €";
        else if (number > 1000000)
            return (number / 1000000).toFixed(2) + "M €";

        return "";
    }

    render() {

        let data_bar = {
            labels: this.state.top5ProductsUnits.map(p => p.name).slice(0, 5),
            datasets: [{
                label: 'units',
                data: this.state.top5ProductsUnits.map(p => p.value).slice(0, 5),
                backgroundColor: "rgba(255, 99, 132, 0.2)"
            }],
            options: {
                title: {
                    display: true,
                    position: 'top',
                    text: 'Total Sales'
                }
            }
        }

        let data_line_graph = {
            labels: this.state.productsSaleCost.map(p => p.name).slice(0, 10),
            datasets: [{
                label: 'PVP1',
                data: this.state.productsSaleCost.map(p => p.sale).slice(0, 10),
                backgroundColor: "rgba(255, 99, 132, 0.2)"
            }, {
                label: 'Cost',
                data: this.state.productsSaleCost.map(p => p.cost).slice(0, 10),
                backgroundColor: "rgba(54, 162, 235, 0.2)"
            }]
        }


        let data_pie = {
            labels: this.state.top10Products.map(p => p.name).slice(0, 10),
            datasets: [{
                label: '# of Votes',
                data: this.state.top10Products.map(p => p.value).slice(0, 10),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(0, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 199, 255, 0.2)',
                    'rgba(255, 0, 255, 0.2)',
                    'rgba(100, 159, 64, 0.2)',
                    'rgba(210, 59, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(0, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 199, 255, 0.2)',
                    'rgba(255, 0, 255, 0.2)',
                    'rgba(100, 159, 64, 0.2)',
                    'rgba(210, 59, 64, 0.2)'
                ],
                borderWidth: 1
            }]
        }

        return (
            <div>
                <Grid columns={2}>
                    <Grid.Row className="first-row-dropdown">
                        <Grid.Column width={13}>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Select
                                className="align-left dropdown"
                                value={this.state.selectedMonth}
                                onChange={this.handleMonthChange}
                                options={this.state.months}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row stretched >
                        <Grid.Column width={6}>
                            <Segment height={15}>
                                <h4>Top 10 Products</h4>
                                <Doughnut
                                    data={data_pie}
                                    width={30}
                                    height={15}
                                />
                            </Segment>
                        </Grid.Column>
                        <GridColumn width={10}>
                            <Segment height={15}>
                                <ReactTable
                                    data={this.props.products}
                                    columns={[
                                        {
                                            Header: "Product ID",
                                            accessor: "Artigo",
                                            Cell: ({ row }) =>
                                                (<Link to={{
                                                    pathname: `/products/${row.Artigo}`,
                                                    state: { prodInfo: this.getProdInfo(row.Artigo), sales: this.state.sales}
                                                }}>
                                                    {row.Artigo}
                                                </Link>)
                                        },
                                        {
                                            Header: "Product Name",
                                            accessor: "Descricao",
                                        },
                                        {
                                            Header: "Stock",
                                            accessor: "StkActual"
                                        },
                                        {
                                            Header: "Cost Price",
                                            accessor: "PCMedio"
                                        },
                                        {
                                            Header: "Sale Price",
                                            accessor: "PVP1"
                                        }

                                    ]}
                                    defaultPageSize={8}
                                    className="-striped -highlight"
                                />
                            </Segment>
                        </GridColumn>
                    </Grid.Row>
                    <GridRow stretched >
                        <Grid.Column width={6}>
                            <Segment style={{ height: 300 }}>
                                <HorizontalBar
                                    data={data_bar}
                                    width={30}
                                    height={30}
                                    options={{
                                        maintainAspectRatio: false,
                                        title: {
                                            display: true,
                                            position: 'top',
                                            text: 'Top 5 Products Purchased by Customers',
                                            fontStyle: 'bold',
                                            fontSize: 15
                                        }
                                    }}
                                />
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Segment style={{ height: 150 }} >
                                <h4 style={{ height: 100 }}>Inventory Value</h4>
                                <p className="kpi">{this.getPrettyNumber(this.props.inventoryValue)}</p>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width={7}>
                            <Segment>
                                <Line
                                    data={data_line_graph}
                                    width={10}
                                    height={100}
                                    options={{
                                        maintainAspectRatio: false,
                                        title: {
                                            display: true,
                                            position: 'top',
                                            text: 'Top 1o Products Sale Price vs Cost',
                                            fontStyle: 'bold',
                                            fontSize: 15
                                        }
                                    }}
                                />
                            </Segment>
                        </Grid.Column>
                    </GridRow>
                </Grid>
            </div>
        );
    }
}


export default ProductsDashboard;