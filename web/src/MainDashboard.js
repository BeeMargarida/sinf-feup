import React, { Component } from 'react';
import { Grid, Segment } from 'semantic-ui-react';
import { Pie, Line } from 'react-chartjs-2';
import './MainDashboard.css';
import Select from 'react-select';

import axios from 'axios';

let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

class MainDashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            nOpenSales: 0,
            openSalesValue: 0,
            sales: 0,
            products: [],
            revenue: 0,
            cogs: 0,
            ebitda: 0,
            salesValuePerCustomer: [],
            activeCustomers: 0,
            inventoryValue: 0,
            months: [
                { label: "January", value: 0 },
                { label: "February", value: 1 },
                { label: "March", value: 2 },
                { label: "April", value: 3 },
                { label: "May", value: 4 },
                { label: "June", value: 5 },
                { label: "July", value: 6 },
                { label: "August", value: 7 },
                { label: "September", value: 8 },
                { label: "October", value: 9 },
                { label: "November", value: 10 },
                { label: "December", value: 11 }
            ],
            salesYear: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            selectedMonth: { label: months[new Date().getMonth()], value: new Date().getMonth() }
        }

        this.handleMonthChange = this.handleMonthChange.bind(this);
        this.getSalesFromApi = this.getSalesFromApi.bind(this);
        this.getActiveCustomers = this.getActiveCustomers.bind(this);
        this.getInventoryValue = this.getInventoryValue.bind(this);
        this.getProducts = this.getProducts.bind(this);
        this.getOpenSalesFromApi = this.getOpenSalesFromApi.bind(this);
        this.getRevenueFromApi = this.getRevenueFromApi.bind(this);
        this.getGrossProfitMargin = this.getGrossProfitMargin.bind(this);
        this.getCOGSFromApi = this.getCOGSFromApi.bind(this);
        this.getEBITDAFromApi = this.getEBITDAFromApi.bind(this);
        this.getSalesValuePerCustomer = this.getSalesValuePerCustomer.bind(this);

    }

    async componentDidMount() {
        await this.getProducts();
        this.getBalanceteFromApi();
        this.getSalesFromApi();
        this.getActiveCustomers();
        this.getOpenSalesFromApi();
        this.getCOGSFromApi();
        this.getRevenueFromApi();
        this.getEBITDAFromApi();
        this.getSalesValuePerCustomer();
    }

    async componentDidUpdate(prevProps, prevState) {
        if (prevProps.token !== this.props.token) {
            await this.getProducts();
        }

        if (prevState.selectedMonth.label !== this.state.selectedMonth.label) {
            let currentDate = new Date();
            currentDate.setMonth(this.state.selectedMonth.value);
            console.log(currentDate);
            this.getBalanceteFromApi();
            this.getActiveCustomers();
            this.getSalesFromApi();
            this.getOpenSalesFromApi();
            this.getCOGSFromApi();
            this.getRevenueFromApi();
            this.getEBITDAFromApi();
            this.getSalesValuePerCustomer();
        }
    }

    getBalanceteFromApi(){
        let self = this;

        axios.get('http://localhost:3030/getBalancete')
        .then(function(response) {
            self.setState({ hasBS: true });
        })
        .catch(function(error) {
            console.log(error);
            self.setState({ hasBS: false });
        });
    }

    getEBITDAFromApi() {
        let self = this;

        console.log(this.state.selectedMonth.value);

        axios.get('http://localhost:3030/getEBITDA?month=' + this.state.selectedMonth.value)
            .then(function (response) {
                self.setState({ ebitda: response.data });
                console.log(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    handleMonthChange(event) {
        this.setState({ selectedMonth: event });
    }

    getActiveCustomers() {
        let self = this;

        axios.get('http://localhost:3030/getActiveCustomers?month=' + this.state.selectedMonth.value)
            .then(function (response) {
                //console.log(response);

                self.setState({ activeCustomers: response.data });

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getSalesFromApi() {
        let self = this;
        axios.get('http://localhost:3030/getSalesVolume')
            .then(function (response) {
                let salesPerYear = [];
                let currSales = 0;
                for (let i = 0; i < response.data.length; i++) {
                    currSales += response.data[i];
                    salesPerYear.push(response.data[i]);
                }
                self.setState({ sales: currSales });
                self.setState({ salesYear: salesPerYear });
                console.log(currSales);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getSalesValuePerCustomer() {

        axios.get('http://localhost:3030/getSalesValuePerCustomer?month=' + this.state.selectedMonth.value)
            .then((res) => {
                let aux = [];

                for (let i = 0; i < res.data.length; i++) {
                    let exist = false;
                    for (let k = 0; k < aux.length; k++) {
                        if (res.data[i].idCustomer === aux[k].idCustomer) {  //checks if the customer is already in aux array
                            aux[k].saleValue += res.data[i].saleValue;
                            exist = true;
                        }
                    }

                    if (!exist) {
                        aux.push(res.data[i]);
                    }
                }

                //orders the array (Descending Order)
                aux.sort(function (a, b) {
                    let x = a.saleValue;
                    let y = b.saleValue;
                    if (x < y) {
                        return 1;
                    } else {
                        return -1;
                    }
                });

                this.setState({ salesValuePerCustomer: aux })
            })
            .catch((err) => console.log(err))

    }

    getOpenSalesFromApi() {
        let self = this;
        axios.get('http://localhost:3030/getOpenSales?month=' + this.state.selectedMonth.value)
            .then(function (response) {
                console.log(response);
                let value = 0;
                for (let i = 0; i < response.data.length; i++) {
                    value += response.data[i].NetTotal;
                }

                self.setState({ nOpenSales: response.data.length });
                self.setState({ openSalesValue: value });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    getCOGSFromApi() {
        let self = this;
        axios.get('http://localhost:3030/getCOGS?month=' + this.state.selectedMonth.value)
            .then(function (response) {
                self.setState({ cogs: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getRevenueFromApi() {
        let self = this;

        axios.get('http://localhost:3030/getRevenue?month=' + this.state.selectedMonth.value)
            .then(function (response) {
                self.setState({ revenue: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getGrossProfitMargin() {
        let gpm = (this.state.revenue[this.state.selectedMonth.value] - this.state.cogs) / this.state.revenue[this.state.selectedMonth.value];

        return isNaN(gpm) ? 0 : ((gpm * 100).toFixed(2) + "%");
    }

    async getProducts() {
        await axios({
            method: 'POST',
            url: "http://localhost:2018/WebApi/Administrador/Consulta",
            data: "\"SELECT V_INV_ArtigoArmazem.Artigo, Artigo.Descricao, Artigo.PCMedio, ISNULL(SUM(V_INV_ArtigoArmazem.StkActual), 0) AS StkActual, ArtigoMoeda.PVP1 " +
                "FROM V_INV_ArtigoArmazem INNER JOIN ArtigoMoeda ON ArtigoMoeda.Artigo = V_INV_ArtigoArmazem.Artigo INNER JOIN Artigo ON Artigo.Artigo = V_INV_ArtigoArmazem.Artigo " +
                "GROUP BY V_INV_ArtigoArmazem.Artigo, Artigo.Descricao, Artigo.PCMedio, ArtigoMoeda.PVP1 ORDER BY V_INV_ArtigoArmazem.Artigo\"",
            headers: {
                'Authorization': "bearer " + this.props.token,
                "Content-Type": "application/json"
            },
        })
            .then((res) => {
                this.setState({ products: res.data.DataSet.Table });
                this.getInventoryValue();
            })
            .catch((err) => {
                console.log(err);
            })

    }

    async getInventoryValue() {
        let inventoryValue = 0;
        for (let i = 0; i < this.state.products.length; i++) {

            if (this.state.products[i].StkActual > 0)
                inventoryValue += this.state.products[i].PCMedio * this.state.products[i].StkActual;

        }
        inventoryValue = inventoryValue.toFixed(2);
        this.setState({ inventoryValue });
    }

    getPrettyNumber(number) {
        if (number < 1000)
            return number.toFixed(2) + " €";
        else if (number > 1000 && number < 1000000)
            return (number / 1000).toFixed(2) + "K €";
        else if (number > 1000000)
            return (number / 1000000).toFixed(2) + "M €";

        return "";
    }

    render() {
        let data = {
            labels: this.state.salesValuePerCustomer.map(p => p.nameCustomer).slice(0, 10),
            datasets: [{
                label: '# of Votes',
                data: this.state.salesValuePerCustomer.map(p => p.saleValue).slice(0, 10),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 98, 0.2)',
                    'rgba(255, 199, 64, 0.2)',
                    'rgba(100, 159, 64, 0.2)',
                    'rgba(210, 59, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 159, 98, 0.2)',
                    'rgba(255, 199, 64, 0.2)',
                    'rgba(100, 159, 64, 0.2)',
                    'rgba(210, 59, 64, 0.2)'
                ],
                borderWidth: 1
            }]
        }

        let data_line_graph = {
            labels: ['January', 'February', 'March', 'April', 'Mars', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            datasets: [{
                label: 'Sales',
                data: this.state.salesYear,
                backgroundColor: "rgba(255, 99, 132, 0.2)"
            }],
            options: {
                title: {
                    display: true,
                    position: 'top',
                    text: 'Avg. Price &amp; Units per Transaction'
                }
            }
        }

        return (
            <div>

                <Grid columns={3} >

                    <Grid.Row className="first-row-dropdown">
                        <Grid.Column width={13}>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Select
                                className="align-left dropdown"
                                value={this.state.selectedMonth}
                                onChange={this.handleMonthChange}
                                options={this.state.months}
                            />
                        </Grid.Column>
                    </Grid.Row>

                    <Grid.Row stretched>

                        <Grid.Column>
                            <Segment>
                                <h4>Top 10 Customers</h4>
                                <Pie
                                    data={data}
                                    width={30}
                                    height={30}
                                />
                            </Segment>
                            <Segment>
                                <h4>Inventory Value</h4>
                                <p className="kpi">{this.getPrettyNumber(this.state.inventoryValue)}</p>
                            </Segment>

                        </Grid.Column>

                        <Grid.Column>
                            <Segment>
                                <h4>EBITDA</h4>
                                <p className="kpi">{this.getPrettyNumber(this.state.ebitda)}</p>
                            </Segment>
                            <Segment>
                                <h4>Active Customers</h4>
                                <p className="kpi">{this.state.activeCustomers}</p>
                            </Segment>
                            <Segment>
                                <h4>Open Sales</h4>

                                <Grid columns={2} style={{ height: 10 }}>
                                    <Grid.Column style={{ height: 30 }}>
                                        <h5>Number</h5>
                                        <p className="kpi">{this.state.nOpenSales}</p>
                                    </Grid.Column>
                                    <Grid.Column style={{ height: 30 }}>
                                        <h5>Value</h5>
                                        <p className="kpi">{this.getPrettyNumber(this.state.openSalesValue)}</p>
                                    </Grid.Column>
                                </Grid>
                            </Segment>
                        </Grid.Column>

                        <Grid.Column>
                            <Segment >
                                <h4>Financial Overview</h4>
                                <Segment style={{ height: 350 }}>
                                    <Line
                                        data={data_line_graph}
                                        width={10}
                                        height={10}
                                        options={{
                                            maintainAspectRatio: false,
                                            title: {
                                                display: true,
                                                text: 'Monthly Sales',
                                                fontStyle: 'bold',
                                                fontSize: 15
                                            }
                                        }}
                                    />
                                </Segment>
                                <Segment>
                                    <h4 style={{ height: 30 }}>Sales</h4>
                                    <p className="kpi">{this.getPrettyNumber(this.state.sales)}</p>
                                </Segment>
                                <Segment>
                                    <h4 style={{ height: 30 }}>Gross Margin</h4>
                                    <p className="kpi">{this.getGrossProfitMargin()}</p>
                                </Segment>

                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );

    }

}

export default MainDashboard;