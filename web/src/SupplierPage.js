import React, { Component } from 'react';
import { Grid, Segment, Card } from 'semantic-ui-react';
import ReactTable from "react-table";
import { Link } from 'react-router-dom';
import axios from 'axios';

class SupplierPage extends Component {

    constructor(props) {
        super(props);

        console.log(props.location.state.supplierInfo);

        this.state = {
            id: props.location.state.supplierInfo.Fornecedor,
            info: null,
            supplierProduct: []
        }

        this.getSupplierInfo = this.getSupplierInfo.bind(this);
        this.getSupplierProducts = this.getSupplierProducts.bind(this);

    }

    async componentDidMount() {
        if(this.props.token !== null){
            await this.getSupplierInfo();
            await this.getSupplierProducts();
        }
        
    }

    componentDidUpdate(prevProps) {
        if (prevProps.token !== this.props.token) {
        }
    }

    async getSupplierInfo() {

        await axios({
            method: 'POST',
            url: "http://localhost:2018/WebApi/Administrador/Consulta",
            data: "\"SELECT * FROM Fornecedores WHERE Fornecedor='" + this.state.id + "'\"",
            headers: {
                'Authorization': "bearer " + this.props.token,
                "Content-Type": "application/json"
            },
        })
            .then((res) => {
                this.setState({ info: res.data.DataSet.Table[0] });
            })
            .catch((err) => {
                console.log(err);
            })

    }

    async getSupplierProducts() {

        await axios({
            method: 'POST',
            url: "http://localhost:2018/WebApi/Administrador/Consulta",
            data: "\"SELECT Artigo, LinhasCompras.DataDoc, LinhasCompras.DataEntrada, LinhasCompras.DataEntrega, LinhasCompras.Quantidade, " +
                "LinhasCompras.PrecUnit, LinhasCompras.PrecoLiquido, Fornecedores.Nome FROM LinhasCompras INNER JOIN CabecCompras ON LinhasCompras.IdCabecCompras = " +
                "CabecCompras.Id INNER JOIN Fornecedores ON CabecCompras.Entidade = Fornecedores.Fornecedor WHERE Fornecedor = '" + this.state.id + "' AND Artigo <> 'null'\"",
            headers: {
                'Authorization': "bearer " + this.props.token,
                "Content-Type": "application/json"
            },
        })
            .then((res) => {
                let prods = res.data.DataSet.Table.map(prod => {
                    return {...prod, PrecUnit: prod.PrecUnit.toFixed(2), PrecoLiquido: prod.PrecoLiquido.toFixed(2), Quantidade: parseInt(prod.Quantidade)}
                });

                console.log(res.data.DataSet.Table);
                this.setState({ supplierProduct: prods });
            })
            .catch((err) => {
                console.log(err);
            })

    }

    getProdInfo(prodName) {
        for (let i = 0; i < this.props.products.length; i++) {
            if (this.props.products[i].Artigo === prodName) {
                return { PVP1: parseFloat(this.props.products[i].PVP1).toFixed(2), StkActual: this.props.products[i].StkActual }
            }
        }
    }


    getPrettyNumber(number) {
        if (number < 1000)
            return number.toFixed(2) + " €";
        else if (number > 1000 && number < 1000000)
            return (number / 1000).toFixed(2) + "K €";
        else if (number > 1000000)
            return (number / 1000000).toFixed(2) + "M €";

        return "";
    }


    render() {

        if (this.state.info === null) {
            return (
                <div className="loading">
                    <div className="loader"></div>
                </div>
            );
        }

        return (
            <div>
                 <Grid columns={2}>
                    <Grid.Row stretched>
                        <Grid.Column width={6}>
                            <Segment>
                                <h2>{this.state.info.Nome}</h2>
                                <p><strong>Supplier ID: </strong> {this.state.info.Fornecedor}</p>
                                <p><strong>Fiscal Name: </strong> {this.state.info.NomeFiscal}</p>
                                <Card className="align_left">
                                    <Card.Content>
                                        <Card.Header>Information</Card.Header>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <p><strong>Address Detail: </strong>{this.state.info.Morada}</p>
                                        <p><strong>City: </strong>{this.state.info.CpLoc}</p>
                                        <p><strong>Taxpayer Number: </strong>{this.state.info.NumContrib}</p>
                                        <p><strong>Telephone: </strong>{this.state.info.Tel}</p>
                                        <p><strong>Fax: </strong>{this.state.info.Fax}</p>
                                        <p><strong>Web Address: </strong>{this.state.info.EnderecoWeb}</p>
                                    </Card.Content>
                                </Card>
                            </Segment>

                            <Segment style={{ height: 150 }} >
                                <h4 style={{ height: 50 }}>Total Debit to Supplier</h4>
                                <p className="kpi">{this.getPrettyNumber(this.state.info.TotalDeb)}</p>
                            </Segment>

                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Segment height={10}>
                                <ReactTable
                                    data={this.state.supplierProduct}
                                    columns={[
                                        {
                                            Header: "Product ID",
                                            accessor: "Artigo",
                                            Cell: ({ row }) =>
                                                (<Link to={{
                                                    pathname: `/products/${row.Artigo}`,
                                                    state: { prodInfo: this.getProdInfo(row.Artigo), sales: this.props.location.state.sales }
                                                }}>
                                                    {row.Artigo}
                                                </Link>)
                                        },
                                        {
                                            Header: "Entry Date",
                                            accessor: "DataEntrada",
                                        },
                                        {
                                            Header: "Delivery Date",
                                            accessor: "DataEntrega"
                                        },
                                        {
                                            Header: "Unit Price €",
                                            accessor: "PrecUnit"
                                        },
                                        {
                                            Header: "Quantity",
                                            accessor: "Quantidade"
                                        },
                                        {
                                            Header: "Liquid Price €",
                                            accessor: "PrecoLiquido"
                                        }

                                    ]}
                                    defaultPageSize={8}
                                    className="-striped -highlight"
                                />
                            </Segment>
                            <Segment style={{ height: 150 }} >
                                <h4 style={{ height: 50 }}>Pendent Orders Value</h4>
                                <p className="kpi">{this.getPrettyNumber(this.state.info.EncomendasPendentes)}</p>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );

    }

}


export default SupplierPage;