import React, { Component } from 'react';
import { Grid, Menu, Segment } from 'semantic-ui-react';
import { Route, NavLink, Redirect } from 'react-router-dom';
import Navbar from './Navbar';
import MainDashboard from './MainDashboard';
import SalesDashboard from './SalesDashboard';
import FinancialDashboard from './FinancialDashboard';
import ProductsDashboard from './ProductsDashboard';
import ProductsPage from './ProductsPage';
import SalesPage from './SalesPage';
import ClientPage from './ClientPage';
import SupplierPage from './SupplierPage';
import LoginForm from './LoginForm';
import axios from 'axios';
import qs from 'qs';
import './App.css';

const Nav = props => (
  <NavLink
    exact
    {...props}
    activeClassName="active"
  />
);

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      token: null,
      token_type: null,
      expires_token: null,
      asking_token: true,
      hasSaft: false,
      products: [],
      inventoryValue: 0
    }

    this.getToken = this.getToken.bind(this);
    this.logOut = this.logOut.bind(this);
    this.getProducts = this.getProducts.bind(this);
    this.getInventoryValue = this.getInventoryValue.bind(this);

  }

  async componentDidMount() {

    axios.get('http://localhost:3030/isValid?fileName="SAFT_DEMO_NEW.xml"')
      .then((res) => {

        if (res.data.includes("Valid")) {
          axios.get('http://localhost:3030/getFile?fileName="SAFT_DEMO_NEW"')
            .then(() => { this.setState({ hasSaft: true }) })
            .catch((err) => console.log(err));

        }
      })
      .catch((err) => console.log(err));

    let token = window.localStorage.getItem("token");
    if (token !== null) {
      this.setState({ token }, await this.getProducts);
    }
  }

  async getToken(username, password, company) {

    this.setState({ asking_token: true }, async () => {

      await axios({
        method: 'POST',
        url: "http://localhost:2018/WebApi/token",
        data: qs.stringify({
          username: username,
          password: password,
          company: company,
          instance: "Default",
          grant_type: "password",
          line: "Professional"
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
        .then(async (res) => {
          console.log(res.data);
          this.setState({ token: res.data.access_token, token_type: res.data.token_type, expires_token: res.data.expires_in, asking_token: false });
          window.localStorage.setItem("token", res.data.access_token);

          await this.getProducts();

          return true;
        })
        .catch((err) => {
          this.setState({ asking_token: false });

          if (err.response.status >= 500)
            this.getToken(username, password, company); //retry 
          else if (err.response.status >= 400) {
            //here shows a message sayng that the credentials were incorrect
          }

          return false;
        })

    });

  }

  async getProducts() {

    await axios({
      method: 'POST',
      url: "http://localhost:2018/WebApi/Administrador/Consulta",
      data: "\"SELECT V_INV_ArtigoArmazem.Artigo, Artigo.Descricao, Artigo.PCMedio, ISNULL(SUM(V_INV_ArtigoArmazem.StkActual), 0) AS StkActual, ArtigoMoeda.PVP1 " +
        "FROM V_INV_ArtigoArmazem INNER JOIN ArtigoMoeda ON ArtigoMoeda.Artigo = V_INV_ArtigoArmazem.Artigo INNER JOIN Artigo ON Artigo.Artigo = V_INV_ArtigoArmazem.Artigo " +
        "GROUP BY V_INV_ArtigoArmazem.Artigo, Artigo.Descricao, Artigo.PCMedio, ArtigoMoeda.PVP1 ORDER BY V_INV_ArtigoArmazem.Artigo\"",
      headers: {
        'Authorization': "bearer " + this.state.token,
        "Content-Type": "application/json"
      },
    })
      .then((res) => {
        let prods = res.data.DataSet.Table.map(prod => {
          return { ...prod, PCMedio: prod.PCMedio.toFixed(2), PVP1: prod.PVP1.toFixed(2) }
        })
        this.setState({ products: prods });
        this.getInventoryValue();
      })
      .catch((err) => {
        console.log(err);
      })

  }

  async getInventoryValue() {

    let inventoryValue = 0;
    for (let i = 0; i < this.state.products.length; i++) {

      if (this.state.products[i].StkActual > 0)
        inventoryValue += this.state.products[i].PCMedio * this.state.products[i].StkActual;

    }

    inventoryValue = inventoryValue.toFixed(2);
    this.setState({ inventoryValue });
  }

  //deletes token saved 
  logOut() {

    this.setState({
      token: null,
      token_type: null,
      expires_token: null,
      asking_token: false
    });
    window.localStorage.setItem("token", null);
    return true;
  }

  render() {

    if (this.state.token === null) {
      return (
        <div className="App">
          <Route exact path='/' render={() =>
            <LoginForm getToken={this.getToken} />
          } />
        </div>
      )
    }

    return (
      <div className="App">
        <Navbar logOut={this.logOut} />
        <Grid>
          <Grid.Column width={2}>
            <Menu fluid vertical tabular>
              <Menu.Item name='main' as={Nav} to='/main' />
              <Menu.Item name='sales' as={Nav} to='/sales' />
              <Menu.Item name='products' as={Nav} to='/products' />
              <Menu.Item name='financial' as={Nav} to='/financial' />
            </Menu>
          </Grid.Column>

          <Grid.Column stretched width={14}>
            <Segment>

              <Route exact path="/" render={() =>
                <Redirect
                  to="/main"
                />
              }
              />
              <Route exact path='/main' render={() =>
                <MainDashboard token={this.state.token} />
              } />

              <Route exact path='/sales' render={() =>
                <SalesDashboard token={this.state.token} products={this.state.products} />
              } />

              <Route path='/sales/:id' render={props =>
                <SalesPage token={this.state.token} {...props} products={this.state.products} /> //get StkActual and Price from products array
              } />

              <Route path='/supplier/:id' render={props =>
                <SupplierPage token={this.state.token} {...props} products={this.state.products} /> //get StkActual and Price from products array
              } />

              <Route path='/customers/:id' render={props =>
                <ClientPage token={this.state.token} {...props} /> //get StkActual and Price from products array
              } />

              <Route path='/financial' render={() =>
                <FinancialDashboard token={this.state.token} hasSaft={this.state.hasSaft} />
              } />

              <Route path='/products/:id' render={props =>
                <ProductsPage token={this.state.token} {...props} products={this.state.products} /> //get StkActual and Price from products array
              } />

              <Route exact path='/products' render={() =>
                <ProductsDashboard token={this.state.token} products={this.state.products} inventoryValue={this.state.inventoryValue} />
              } />

            </Segment>
          </Grid.Column>
        </Grid>

      </div>
    );
  }
}

export default App;
