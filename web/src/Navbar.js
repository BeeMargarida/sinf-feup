import React, { Component } from 'react';
import { Button, Menu } from 'semantic-ui-react'
import { Route } from 'react-router-dom';
import './Navbar.css';

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        }
        this.logOut = this.logOut.bind(this);
    }

    logOut(history) {
        this.props.logOut();
        history.push('/');
    }

    render() {

        let button = () => (
            <Route render={({ history}) => (
              <Button
                type='button'
                onClick={() => { this.logOut(history) }}
              >
                Log Out
              </Button>
            )} />
          )

        return (
            <Menu size='large' inverted>
                <h2 id="navbar-brand">Home</h2>

                <Menu.Menu position='right'>
                    <Menu.Item>
                        {button()}
                    </Menu.Item>
                </Menu.Menu>
            </Menu>
        );

    }

}

export default Navbar;