const express = require('express');
const app = express();
const { exec } = require('child_process');
const fs = require('file-system');
const cors = require('cors');

app.use(cors());

let saftFile;

let balancete = [
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
    { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } }
];

function resetBalancete() {
    balancete = [
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } },
        { 11: { D: 0, C: 0 }, 12: { D: 0, C: 0 }, 21: { D: 0, C: 0 }, 22: { D: 0, C: 0 }, 24: { D: 0, C: 0 }, 31: { D: 0, C: 0 }, 32: { D: 0, C: 0 }, 61: { D: 0, C: 0 }, 62: { D: 0, C: 0 }, 63: { D: 0, C: 0 }, 71: { D: 0, C: 0 }, 72: { D: 0, C: 0 }, 9: { D: 0, C: 0 } }
    ];
}

let sales = 0;
let revenue = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

const port = 3030;

app.listen(port);

app.get('/isValid', (req, res) => {

    let cmd = 'cd ../saft-java && java -jar Untitled.jar ' + req.query.fileName;
    exec(cmd, (error, stdout, stderr) => {
        if (stdout == "Valid\n") {
            return res.status(200).send(stdout);
        }
        else if (stdout == "Not valid\n")
            return res.status(400).send(stdout);
    });
});

app.get('/getFile', (req, res) => {

    fs.readFile('../assets/' + req.query.fileName.split("\"")[1] + '.json', 'utf8', (err, data) => {
        if (err) throw err;
        saftFile = JSON.parse(data)
        // preencherBalancete();
        return res.status(200).send(saftFile);
    });
});

app.get('/listProducts', (req, res) => {
    return res.status(200).send(saftFile.AuditFile.MasterFiles.Product);
});

app.get('/listCustomers', (req, res) => {
    return res.status(200).send(saftFile.AuditFile.MasterFiles.Customer);
});


function fillMonthlyBalanceSheet() {
    resetBalancete();
    let generalLedgerEntries = saftFile.AuditFile.GeneralLedgerEntries.Journal;

    for (entry in generalLedgerEntries) {
        handleEntry(generalLedgerEntries[entry]);
    }
}

function handleEntry(entry) {
    let transaction = entry.Transaction;

    if (transaction == null)
        return;

    if (transaction.length == null) {
        handleTransaction(transaction);
    } else {
        for (t in transaction) {
            handleTransaction(transaction[t]);
        }
    }
}

app.get('/getBalancete', (req, res) => {

    fillMonthlyBalanceSheet(req.query.month);

    return res.status(200).send(balancete);
});

app.get('/getCash', (req, res) => {
    let month = parseInt(req.query.month);
    let cash = balancete[month][11].D - balancete[month][11].C;

    return res.status(200).send("" + cash.toFixed(2));
});

app.get('/getEBITDA', (req, res) => {
    let month = parseInt(req.query.month);
    let c71 = balancete[month][71].C - balancete[month][71].D;
    let c72 = balancete[month][72].C - balancete[month][72].D;
    let c61 = balancete[month][61].D - balancete[month][61].C;
    let c62 = balancete[month][62].D - balancete[month][62].C;
    let c63 = balancete[month][63].D - balancete[month][63].C;

    let ebitda = c71 + c72 - c61 - c62 - c63;

    return res.status(200).send("" + ebitda.toFixed(2));
});

app.get('/getBank', (req, res) => {
    let month = parseInt(req.query.month);
    let bank = balancete[month][12].D - balancete[month][12].C;
    return res.status(200).send("" + bank.toFixed(2));
});

app.get('/getAP', (req, res) => {
    let month = parseInt(req.query.month);
    let ap = balancete[month][22].C - balancete[month][22].D;
    return res.status(200).send("" + ap);
});

app.get('/getAR', (req, res) => {
    let month = parseInt(req.query.month);
    let ar = balancete[month][21].D - balancete[month][21].C;
    return res.status(200).send("" + ar.toFixed(2));
});

app.get('/getCOGS', (req, res) => {
    let month = parseInt(req.query.month);
    let cogs = balancete[month][61].D - balancete[month][61].C;
    return res.status(200).send("" + cogs.toFixed(2));
});

app.get('/getSalesVolume', (req, res) => {

    let salesInvoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;
    let salesVolume = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (let i = 0; i < salesInvoices.length; i++) {
        let invoiceMonth = salesInvoices[i].InvoiceDate.split('-')[1];
        salesVolume[invoiceMonth - 1] += salesInvoices[i].DocumentTotals.NetTotal;
    }
    return res.status(200).send(salesVolume);
});

app.get('/getSalesValuePerCustomer', (req, res) => {

    let month = req.query.month;

    let salesInvoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;
    let customers = saftFile.AuditFile.MasterFiles.Customer;

    let salesValuePerCustomer = [];
    for (let i = 0; i < customers.length; i++) {

        for (let j = 0; j < salesInvoices.length; j++) {

            let date = new Date(salesInvoices[j].InvoiceDate);

            if (salesInvoices[j].CustomerID === customers[i].CustomerID && parseInt(date.getMonth()) === parseInt(month)) {

                salesValuePerCustomer.push({
                    nameCustomer: customers[i].CompanyName,
                    idCustomer: customers[i].CustomerID,
                    saleValue: salesInvoices[j].DocumentTotals.NetTotal
                });

            }

        }

    }

    return res.status(200).send(salesValuePerCustomer);
});

app.get('/getAvgUnitsValueTransactionPerMonth', (req, res) => {

    let salesInvoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;

    let result = [
        { units: 0, value: 0, totalT: 0 }, { units: 0, value: 0, totalT: 0 },
        { units: 0, value: 0, totalT: 0 }, { units: 0, value: 0, totalT: 0 },
        { units: 0, value: 0, totalT: 0 }, { units: 0, value: 0, totalT: 0 },
        { units: 0, value: 0, totalT: 0 }, { units: 0, value: 0, totalT: 0 },
        { units: 0, value: 0, totalT: 0 }, { units: 0, value: 0, totalT: 0 },
        { units: 0, value: 0, totalT: 0 }, { units: 0, value: 0, totalT: 0 }
    ];

    for (let i = 0; i < 11; i++) {

        for (let j = 0; j < salesInvoices.length; j++) {

            let date = new Date(salesInvoices[j].InvoiceDate);

            let units = 0;
            for (let l = 0; l < salesInvoices[j].Line.length; l++) {
                units += salesInvoices[j].Line[l].Quantity;
            }

            result[parseInt(date.getMonth())] = {
                units: result[parseInt(date.getMonth())].units + units,
                value: result[parseInt(date.getMonth())].value + salesInvoices[j].DocumentTotals.NetTotal,
                totalT: result[parseInt(date.getMonth())].totalT + 1
            }

        }

    }

    result = result.map(res => ({ units: res.units / res.totalT, value: res.value / res.totalT }));

    return res.status(200).send(result);
});

app.get('/getTop10Products', (req, res) => {

    let month = req.query.month;
    let products = [];

    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;

    for (let i = 0; i < invoices.length; i++) {

        let date = new Date(invoices[i].InvoiceDate);

        if (parseInt(date.getMonth()) === parseInt(month)) {

            for (let j = 0; j < invoices[i].Line.length; j++) {

                products.push({
                    name: invoices[i].Line[j].ProductCode,
                    value: invoices[i].Line[j].CreditAmount
                });

            }
        }

    }

    let sortedProducts = products.sort(function (a, b) {
        return (a.value > b.value) ? -1 : ((b.value > a.value) ? 1 : 0)
    });

    return res.status(200).send(sortedProducts);
});

app.get('/getTop5ProductsUnits', (req, res) => {

    let month = req.query.month;
    let products = [];

    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;

    for (let i = 0; i < invoices.length; i++) {

        let date = new Date(invoices[i].InvoiceDate);

        if (parseInt(date.getMonth()) === parseInt(month)) {

            for (let j = 0; j < invoices[i].Line.length; j++) {

                products.push({
                    name: invoices[i].Line[j].ProductCode,
                    value: invoices[i].Line[j].Quantity
                });

            }
        }

    }

    let sortedProducts = products.sort(function (a, b) {
        return (a.value > b.value) ? -1 : ((b.value > a.value) ? 1 : 0)
    });

    return res.status(200).send(sortedProducts);
});

app.get('/getSales', (req, res) => {

    let month = req.query.month;
    let sales = [];

    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;
    let clients = saftFile.AuditFile.MasterFiles.Customer;

    for (let i = 0; i < invoices.length; i++) {

        let date = new Date(invoices[i].InvoiceDate);

        if (parseInt(date.getMonth()) === parseInt(month)) {

            let clientName;
            for (let j = 0; j < clients.length; j++) {
                if (clients[j].CustomerID === invoices[i].CustomerID) {
                    clientName = clients[j].CompanyName;
                }
            }
            sales.push({ ...invoices[i], clientName: clientName });

        }
    }

    return res.status(200).send(sales);
});

app.get('/getAllSales', (req, res) => {

    let sales = [];

    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;
    let clients = saftFile.AuditFile.MasterFiles.Customer;

    for (let i = 0; i < invoices.length; i++) {

        let clientName;
        for (let j = 0; j < clients.length; j++) {
            if (clients[j].CustomerID === invoices[i].CustomerID) {
                clientName = clients[j].CompanyName;
            }
        }
        sales.push({ ...invoices[i], clientName: clientName });

    }

    return res.status(200).send(sales);
});

app.get('/getOpenSales', (req, res) => {

    let month = req.query.month;
    let openSales = [];

    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;
    let journals = saftFile.AuditFile.GeneralLedgerEntries.Journal;


    for (let i = 0; i < invoices.length; i++) {

        let date = new Date(invoices[i].InvoiceDate);
        let exist = false;

        for (let j = 0; j < journals.length; j++) {

            let transactions = journals[j].Transaction;
            let sourceDocuments = []; //array that will contain all the sourceDocuments of all of the transactions

            if (transactions != null) {

                if (transactions.length == null) {
                    sourceDocuments = sourceDocuments.concat(getAllSourceDocumentIDFromTransaction(transactions.Lines));
                } else {
                    for (t in transactions) {
                        sourceDocuments = sourceDocuments.concat(getAllSourceDocumentIDFromTransaction(transactions[t].Lines));
                    }
                }
            }

            for (s in sourceDocuments) {
                if (invoices[i].InvoiceNo === sourceDocuments[s]) { //checks if there are any sale that wasn't invoiced
                    exist = true;
                }
            }

        }

        if (parseInt(date.getMonth()) === parseInt(month) && !exist) {
            openSales.push({ ...invoices[i].DocumentTotals });
        }
    }

    return res.status(200).send(openSales);
});

function getAllSourceDocumentIDFromTransaction(lines) {

    let creditLines = lines.CreditLine;
    let debitLines = lines.DebitLine;
    let sourceDocuments = [];

    //checks if there are any creditLines
    if (creditLines != null) {

        if (creditLines.length == null)
            sourceDocuments.push(creditLines.SourceDocumentID);
        else {
            for (line in creditLines)
                sourceDocuments.push(creditLines[line].SourceDocumentID);
        }
    }

    //checks if there are any debitLines
    if (debitLines != null) {

        if (debitLines.length == null)
            sourceDocuments.push(debitLines.SourceDocumentID);
        else {
            for (line in debitLines)
                sourceDocuments.push(debitLines[line].SourceDocumentID);
        }
    }

    return sourceDocuments;
}

app.get('/getActiveCustomers', (req, res) => {
    let activeCustomers = [];
    let minimumMonth = Math.max(req.query.month - 3, 0);
    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;

    for (let i = 0; i < invoices.length; i++) {
        let date = new Date(invoices[i].InvoiceDate);
        if (date.getMonth() >= minimumMonth && date.getMonth() <= req.query.month) {
            if (!activeCustomers.includes(invoices[i].CustomerID)) {
                activeCustomers.push(invoices[i].CustomerID);
            }
        }
    }

    let activeCustomersNumber = activeCustomers.length;
    return res.status(200).send("" + activeCustomersNumber);
});

app.get('/getClientInfo', (req, res) => {

    let companyName = req.query.companyName;
    let clientInfo = {};

    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;
    let clients = saftFile.AuditFile.MasterFiles.Customer;

    for (let j = 0; j < clients.length; j++) {

        if (clients[j].CompanyName.includes(companyName)) {
            clientInfo = clients[j];
        }
    }

    let clientInvoices = invoices.filter(invoice => invoice.CustomerID === clientInfo.CustomerID);

    clientInfo.invoices = clientInvoices;

    return res.status(200).send(clientInfo);

});

app.get('/getProductSales', (req, res) => {

    let productId = req.query.id;
    let productSales = [];
    let creditAmount = 0;

    let invoices = saftFile.AuditFile.SourceDocuments.SalesInvoices.Invoice;

    for (let i = 0; i < invoices.length; i++) {

        for (let j = 0; j < invoices[i].Line.length; j++) {

            if (invoices[i].Line[j].ProductCode === productId) {
                productSales.push(invoices[i]);
                creditAmount += invoices[i].Line[j].CreditAmount;
            }

        }
    }

    return res.status(200).send({ sales: productSales, creditAmount });

});

//retorna array com revenue por mês
app.get('/getRevenue', (req, res) => {
    revenue = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let journalEntries = saftFile.AuditFile.GeneralLedgerEntries.Journal;
    let rev = 0;
    for (let i = 0; i < journalEntries.length; i++) {
        rev += handleJournalEntry(journalEntries[i]);
    }

    return res.status(200).send(revenue);
});

function handleJournalEntry(entry) {
    let transaction = entry.Transaction;
    if (transaction == null) {
        return 0;
    }

    let creditAmount = 0;

    if (transaction.length == null) {
        let creditLine = transaction.Lines.CreditLine;
        let month = new Date(transaction.TransactionDate).getMonth();
        creditAmount = handleCreditLines(creditLine, month);
    } else {
        for (let i = 0; i < transaction.length; i++) {
            let creditLine = transaction[i].Lines.CreditLine;
            let month = new Date(transaction[i].TransactionDate).getMonth();
            creditAmount += handleCreditLines(creditLine, month);
        }
    }

    return creditAmount;
}

function handleCreditLines(line, month) {
    if (line.length == null) {
        if (line.AccountID == 7111 || line.AccountID == 7112) {
            revenue[month] += parseFloat(line.CreditAmount);
            return line.CreditAmount;
        } else
            return 0;
    } else {
        let creditAmount = 0;
        for (let i = 0; i < line.length; i++) {
            if (line[i].AccountID == 7111 || line[i].AccountID == 7112) {
                creditAmount += line[i].CreditAmount;
                revenue[month] += parseFloat(line[i].CreditAmount);
            } else
                creditAmount += 0;
        }
        return creditAmount;
    }
}

function handleTransaction(transaction) {
    let creditLines = transaction.Lines.CreditLine;
    let debitLines = transaction.Lines.DebitLine;
    let date = transaction.TransactionDate;
    if (creditLines != null) {
        if (creditLines.length == null)
            handleCreditLine(creditLines, date);
        else {
            for (line in creditLines)
                handleCreditLine(creditLines[line], date);
        }
    }
    if (debitLines != null) {
        if (debitLines.length == null)
            handleDebitLine(debitLines, date);
        else {
            for (line in debitLines)
                handleDebitLine(debitLines[line], date);
        }
    }
}

function handleCreditLine(line, date) {
    let account = "" + line.AccountID;
    let monthNumber = new Date(date).getMonth();
    switch (account[0]) {
        case "1":
            if (account[1] == 1) {
                balancete[monthNumber][11].C += line.CreditAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][12].C += line.CreditAmount;
            }
            break;
        case "2":
            if (account[1] == 1) {
                balancete[monthNumber][21].C += line.CreditAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][22].C += line.CreditAmount;
            } else if (account[1] == 4) {
                balancete[monthNumber][24].C += line.CreditAmount;
            }
            break;
        case "3":
            if (account[1] == 1) {
                balancete[monthNumber][31].C += line.CreditAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][32].C += line.CreditAmount;
            }
            break;
        case "6":
            if (account[1] == 1) {
                balancete[monthNumber][61].C += line.CreditAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][62].C += line.CreditAmount;
            } else if (account[1] == 3) {
                balancete[monthNumber][63].C += line.CreditAmount;
            }
            break;
        case "7":
            if (account[1] == 1) {
                balancete[monthNumber][71].C += line.CreditAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][72].C += line.CreditAmount;
            }
            break;
        case "9":
            balancete[monthNumber][9].C += line.CreditAmount;
            break;
        default: break;
    }
}

function handleDebitLine(line, date) {
    let account = "" + line.AccountID;
    let monthNumber = new Date(date).getMonth();
    switch (account[0]) {
        case "1":
            if (account[1] == 1) {
                balancete[monthNumber][11].D += line.DebitAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][12].D += line.DebitAmount;
            }
            break;
        case "2":
            if (account[1] == 1) {
                balancete[monthNumber][21].D += line.DebitAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][22].D += line.DebitAmount;
            } else if (account[1] == 4) {
                balancete[monthNumber][24].D += line.DebitAmount;
            }
            break;
        case "3":
            if (account[1] == 1) {
                balancete[monthNumber][31].D += line.DebitAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][32].D += line.DebitAmount;
            }
            break;
        case "6":
            if (account[1] == 1) {
                balancete[monthNumber][61].D += line.DebitAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][62].D += line.DebitAmount;
            } else if (account[1] == 3) {
                balancete[monthNumber][63].D += line.DebitAmount;
            }
            break;
        case "7":
            if (account[1] == 1) {
                balancete[monthNumber][71].D += line.DebitAmount;
            } else if (account[1] == 2) {
                balancete[monthNumber][72].D += line.DebitAmount;
            }
            break;
        case "9":
            balancete[monthNumber][9].D += line.DebitAmount;
            break;
        default: break;
    }

}