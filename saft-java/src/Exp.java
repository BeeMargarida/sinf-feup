import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import org.xml.sax.SAXException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Exp {

	public static void main(String[] args) {

		String xmlFileName = args[0];//args[0];
		boolean isValid = false;

		File schemaFile = new File("XSD.xsd");
		File xmlOriginalFile = new File(xmlFileName);
		Source xmlFile = new StreamSource(xmlOriginalFile);
		SchemaFactory schemaFactory = SchemaFactory
				.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		try {
			Schema schema = schemaFactory.newSchema(schemaFile);
			Validator validator = schema.newValidator();
			validator.validate(xmlFile);
			isValid = true;
			System.out.println("Valid");
		} catch (SAXException e) {
			System.out.println("Not valid " + e);
		} catch (IOException e) {
			System.out.println(e);
		}

		JSONObject resultJson = new JSONObject();

		if(isValid) {
			try {
				String xmlString = "";
				xmlString = new String(Files.readAllBytes(Paths.get(xmlFileName)));
				resultJson = parseXMLtoJSON(xmlString);

			} catch (IOException e) {
				e.printStackTrace();
			}


			PrintWriter writer;
			try {
				String fileTitle = "../assets/" + xmlOriginalFile.getName().split(".xml")[0] + ".json";
				writer = new PrintWriter(fileTitle, "UTF-8");
				writer.print(resultJson);
				writer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	}

	private static JSONObject parseXMLtoJSON(String xmlString){
		JSONObject soapDatainJsonObject = null;
		try {
			soapDatainJsonObject = XML.toJSONObject(xmlString);
			return soapDatainJsonObject;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}	
}
